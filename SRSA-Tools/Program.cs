﻿using System.CommandLine;
using SRSA.Tools.Results;
using SRSA.Tools.BalanceOfPerformance;

var rootCommand = new RootCommand
{
    ResultsHost.BuildCommand(),
    BalanceOfPerformanceHost.BuildCommand()
};

rootCommand.Description = "SRSA-Tools";

return await rootCommand.InvokeAsync(args);
