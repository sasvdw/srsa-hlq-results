﻿namespace SRSA.Tools.Integration.LowFuelMotorsport;

using System.Text.Json;
using Flurl.Http;
using Flurl.Http.Configuration;
using Models.Hotlaps;
public class LowFuelMotorsportApiClient
{
    private readonly IFlurlClient flurlClient;
    private const string baseUrl = "https://api2.lowfuelmotorsport.com/api";

    public LowFuelMotorsportApiClient()
    {
        this.flurlClient = new FlurlClient(baseUrl)
            .WithSettings(s =>
            {
                s.JsonSerializer = new DefaultJsonSerializer(new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.SnakeCaseLower
                });
            });
    }

    public HotlapsRequestBuilder Hotlaps => new(flurlClient);

    public class HotlapsRequestBuilder(IFlurlClient flurlClient)
    {
        private const string hotlapsUrlSegment = "hotlaps";
        public WithGetAccBopItemRequestBuilder GetAccBop => new(flurlClient);
        public class WithGetAccBopItemRequestBuilder(IFlurlClient flurlClient)
        {
            private const string getAccBopUrlSegment = "getAccBop";

            public async Task<List<TrackBalanceOfPerformance>> GetAsync()
            {
                var flurlRequest = flurlClient.Request(hotlapsUrlSegment, getAccBopUrlSegment);
                var accBop = await flurlRequest
                    .GetJsonAsync<List<TrackBalanceOfPerformance>>();

                return accBop;
            }
        }
    }
}