﻿namespace SRSA.Tools.Results.Domain;

using System;

public class Driver
{
    public string Name { get; }
    public string Session { get; private set; }
    public uint BestTime { get; private set; }
    public string Car { get; private set; }
    public uint TotalLaps { get; private set; }

    internal Driver(string name, uint bestTime, string session, string car, uint totalLaps)
    {
        this.Name = name;
        this.BestTime = bestTime;
        this.Session = session;
        this.Car = car;
        this.TotalLaps = totalLaps;
    }

    internal void Merge(uint bestTime, string session, string car, uint totalLaps)
    {
        if(bestTime < this.BestTime || 
           bestTime == this.BestTime && string.Compare(session, this.Session, StringComparison.Ordinal) > 0)
        {
            this.BestTime = bestTime;
            this.Session = session;
            this.Car = car;
        }
            
        this.TotalLaps += totalLaps;
    }
}