﻿namespace SRSA.Tools.Results;

using System.Text;
internal class CSVWriter : IDisposable
{
    public void Dispose()
    {
        file?.Dispose();
    }

    private readonly FileStream file;
    public CSVWriter(DirectoryInfo directory, string trackName)
    {
        file = File.Create($"{directory.FullName}\\{trackName}.csv");
    }

    public async Task WriteResults(IEnumerable<ResultLine> results, bool outputDriverIds)
    {
        var headerItems = new List<string>(7);
        headerItems.Add(nameof(ResultLine.Position));
        if (outputDriverIds)
        {
            headerItems.Add(nameof(ResultLine.Id));
        }
        headerItems.Add(nameof(ResultLine.Driver));
        headerItems.Add(nameof(ResultLine.BestTime));
        headerItems.Add(nameof(ResultLine.FromSession));
        headerItems.Add(nameof(ResultLine.UsingCar));
        headerItems.Add(nameof(ResultLine.TotalLapsOverAllSessions));
        var headers = $"{string.Join(',', headerItems)}\n";
        await file.WriteAsync(Encoding.Unicode.GetBytes(headers));

        foreach(var resultLine in results)
        {
            var items = new List<string>(7);
            items.Add(resultLine.Position.ToString());
            if (outputDriverIds)
            {
                items.Add(resultLine.Id);
            }
            items.Add(resultLine.Driver);
            items.Add(resultLine.BestTime.ToString());
            items.Add(resultLine.FromSession);
            items.Add(resultLine.UsingCar);
            items.Add(resultLine.TotalLapsOverAllSessions.ToString());
            var line = $"{string.Join(',', items)}\n";
            await file.WriteAsync(Encoding.Unicode.GetBytes(line));
        }
            
        file.Close();
    }
}

internal record ResultLine
{
    public required string Id { get; init; }
    public uint Position { get; init; }
    public required string Driver { get; init; }
    public TimeSpan BestTime { get; init; }
    public required string FromSession { get; init; }
    public required string UsingCar { get; init; }
    public uint TotalLapsOverAllSessions { get; init; }
}