﻿namespace SRSA.Tools.Integration.SimRacingGP;

using System.Text.Json;
using Flurl.Http;
using Flurl.Http.Configuration;
using Models.Command.BalanceOfPerformance;
using Models.Query.BalanceOfPerformance;
public class SimRacingGPApiClient
{
    private readonly IFlurlClient flurlClient;
    private const string baseUrl = "https://stg-api.simracing.gp/stg";

    public SimRacingGPApiClient(string bearerToken)
    {
        this.flurlClient = new FlurlClient(baseUrl).WithSettings(s =>
        {
            s.JsonSerializer = new DefaultJsonSerializer(new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            });
        }).WithOAuthBearerToken(bearerToken);
    }

    public CommandRequestBuilder Command => new(flurlClient);
    
    public QueryRequestBuilder Query => new(flurlClient);

    public class CommandRequestBuilder(IFlurlClient flurlClient)
    {
        private const string commandUrlSegment = "command";
        public BopRequestBuilder Bop => new(flurlClient);

        public class BopRequestBuilder(IFlurlClient flurlClient)
        {
            private const string bopUrlSegment = "bop";
            public CreateRequestBuilder Create => new(flurlClient);
            public DeleteRequestBuilder Delete => new(flurlClient);
            
            public class CreateRequestBuilder(IFlurlClient flurlClient)
            {
                private const string createUrlSegment = "Create";

                public async Task<AccBalanceOfPefrormanceSummary> PostAsync(NewAccBalanceOfPeformance create)
                {
                    var request = flurlClient.Request(commandUrlSegment, bopUrlSegment, createUrlSegment, 0, 0);

                    var flurlResponse = await request.PostJsonAsync(create);

                    var response = await flurlResponse.GetJsonAsync<AccBalanceOfPefrormanceSummary>();

                    return response;
                }
            }

            public class DeleteRequestBuilder(IFlurlClient flurlClient)
            {
                private const string deleteUrlSegment = "Delete";

                public WithBopItemRequestBuilder this[string bopId] => new(flurlClient, bopId);

                public class WithBopItemRequestBuilder(IFlurlClient flurlClient, string bopId)
                {
                    public async Task<AccBalanceOfPefrormanceSummary> PostAsync()
                    {
                        var request = flurlClient.Request(commandUrlSegment, bopUrlSegment, deleteUrlSegment, bopId, 0);

                        var flurlResponse = await request.PostAsync();
                    
                        var response = await flurlResponse.GetJsonAsync<AccBalanceOfPefrormanceSummary>();

                        return response;
                    }
                }
            }
        } 
    }

    public class QueryRequestBuilder(IFlurlClient flurlClient)
    {
        private const string queryUrlSegment = "query";
        public BopRequestBuilder Bop => new(flurlClient);

        public class BopRequestBuilder(IFlurlClient flurlClient)
        {
            private const string bopUrlSegment = "bop";
            public WithCommunityItemRequestBuilder this[string communityId] => new(flurlClient, communityId);

            public class WithCommunityItemRequestBuilder(IFlurlClient flurlClient, string communityId)
            {
                public AccRequestBuilder Acc => new(flurlClient, communityId);

                public class AccRequestBuilder(IFlurlClient flurlClient, string communityId)
                {
                    private const string accUrlSegment = "acc";

                    public WithBopItemRequestBuilder this[string bopId] => new(flurlClient, communityId, bopId);

                    public async Task<List<AccBalanceOfPerformance>> GetAsync(string? sortFirstTrack = null)
                    {
                        var request = flurlClient.Request(queryUrlSegment, bopUrlSegment, communityId, accUrlSegment)
                            .AppendQueryParam("sort-first-track", sortFirstTrack);

                        var response = await request.GetJsonAsync<List<AccBalanceOfPerformance>>();

                        return response;
                    }

                    public class WithBopItemRequestBuilder(IFlurlClient flurlClient, string communityId, string bopId)
                    {
                        public async Task<List<AccBalanceOfPerformance>> GetAsync()
                        {
                            var request = flurlClient.Request(queryUrlSegment, bopUrlSegment, communityId, accUrlSegment, bopId);

                            var response = await request.GetJsonAsync<List<AccBalanceOfPerformance>>();

                            return response;
                        }
                    }
                }
            }
        }
    }
}