﻿namespace SRSA.Tools.Raw.AC;

using Tools.Results.Domain;

internal class DriverSession: IDriverSession
{
    private readonly Result result;
    public string Id => this.result.DriverGuid;
    public string Name => this.result.DriverName;
    public uint BestTime => this.result.BestLap;
    public string Session { get; }
    public string Car => this.result.CarModel;
    public uint TotalLaps { get; }

    public DriverSession(string session, Result result, uint totalLaps)
    {
        this.Session = session;
        this.result = result;
        this.TotalLaps = totalLaps;
    }
}