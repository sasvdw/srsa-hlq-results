﻿namespace SRSA.Tools.Raw.AC;

using Newtonsoft.Json;

internal class Result
{
    [JsonProperty("DriverName")]
    public string DriverName { get; set; }

    [JsonProperty("DriverGuid")]
    public string DriverGuid { get; set; }

    [JsonProperty("CarId")]
    public int CarId { get; set; }

    [JsonProperty("CarModel")]
    public string CarModel { get; set; }

    [JsonProperty("BestLap")]
    public uint BestLap { get; set; }

    [JsonProperty("TotalTime")]
    public int TotalTime { get; set; }

    [JsonProperty("BallastKG")]
    public int BallastKG { get; set; }

    [JsonProperty("Restrictor")]
    public int Restrictor { get; set; }
}