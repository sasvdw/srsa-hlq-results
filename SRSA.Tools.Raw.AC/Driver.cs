﻿namespace SRSA.Tools.Raw.AC;

using System.Collections.Generic;
using Newtonsoft.Json;

internal class Driver
{
    [JsonProperty("Name")]
    public string Name { get; set; }

    [JsonProperty("Team")]
    public string Team { get; set; }

    [JsonProperty("Nation")]
    public string Nation { get; set; }

    [JsonProperty("Guid")]
    public string Guid { get; set; }

    [JsonProperty("GuidsList")]
    public List<string> GuidsList { get; set; }
}