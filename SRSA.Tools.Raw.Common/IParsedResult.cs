﻿namespace SRSA.Tools.Raw.Common;

using System.Collections.Generic;
using Tools.Results.Domain;
public interface IParsedResult
{
    public string TrackName { get; }

    public IEnumerable<IDriverSession> CreateDriverSessions(string session);
}