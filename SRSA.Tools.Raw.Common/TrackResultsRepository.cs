﻿namespace SRSA.Tools.Raw.Common;

using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Tools.Results.Domain;

public class TrackResultsRepository<TParsedResult>(FileParser<TParsedResult> fileParser, params DirectoryInfo[] directoryInfos) : ITrackResultsRepository
    where TParsedResult : IParsedResult
{
    private readonly IReadOnlyList<DirectoryInfo> directoryInfos = directoryInfos;

    public async Task<IDictionary<string, TrackResults>> GetAllAsync()
    {
        var tracks = new ConcurrentDictionary<string, TrackResults>();

        foreach (var directory in directoryInfos)
        {
            await foreach(var (session, parsedResults) in fileParser.LoadResults(directory))
            {
                if(parsedResults.TrackName == null)
                {
                    continue;
                }

                var trackResults = tracks.GetOrAdd(parsedResults.TrackName, trackName => new TrackResults(trackName));

                var driverSessions = parsedResults.CreateDriverSessions(session);
                trackResults.Merge(driverSessions);
            }
        }

        return tracks;
    }
}