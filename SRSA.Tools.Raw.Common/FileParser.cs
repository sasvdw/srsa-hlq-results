﻿namespace SRSA.Tools.Raw.Common;

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
public class FileParser<TResult>(Encoding encoding) where TResult : IParsedResult
{
    public async IAsyncEnumerable<KeyValuePair<string, TResult>> LoadResults(DirectoryInfo directory)
    {
        var jsonSerializer = JsonSerializer.Create();
        foreach(var file in directory.GetFiles("*.json").OrderBy(x => x.Name))
        {
            await using var fileStream = File.OpenRead(file.FullName);
            using var streamReader = new StreamReader(fileStream, encoding);
            await using var jsonTextReader = new JsonTextReader(streamReader);
            var result = jsonSerializer.Deserialize<TResult>(jsonTextReader);
            yield return new KeyValuePair<string, TResult>(file.Name, result);
        }
    }
}