﻿using NUnit.Framework;

namespace SRSA.Tools.Results.Domain.Tests.Driver
{
    [SetUpFixture]
    public class Spec
    {
        private static (string name, uint bestTime, string session, string car, uint totalLaps) driverDetails;
        private static (uint bestTime, string session, string car, uint totalLaps) improvedSessionTwo;
        private static (uint bestTime, string session, string car, uint totalLaps) worseSessionThree;
        private static (uint bestTime, string session, string car, uint totalLaps) equalSessionFour;
        private static (uint bestTime, string session, string car, uint totalLaps) equalSessionZero;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            var baseTime = 111u;
            driverDetails = ("Driver 1", baseTime, "Test Session 1", "Test Car 1", 10);
            improvedSessionTwo = (baseTime - 1, "Test Session 2", "Test Car 2", 10);
            worseSessionThree = (baseTime + 1, "Test Session 3", "Test Car 3", 10);
            equalSessionFour = (baseTime, "Test Session 4", "Test Car 4", 10);
            equalSessionZero = (baseTime, "Test Session 0", "Test Car 5", 10);
        }

        #region Nested type: Given_created_driver_details_When_merging_driver_with_equal_details_from_new_session

        [TestFixture]
        public class Given_created_driver_details_When_merging_driver_with_equal_details_from_new_session
        {
            private Tools.Results.Domain.Driver driver;

            [SetUp]
            public void Setup()
            {
                (string name, uint bestTime, string session, string car, uint totalLaps) = driverDetails;
                this.driver = new Tools.Results.Domain.Driver(name, bestTime, session, car, totalLaps);

                (bestTime, session, car, totalLaps) = equalSessionFour;
                this.driver.Merge(bestTime, session, car, totalLaps);
            }

            [Test]
            public void Then_car_is_updated()
            {
                Assert.AreEqual(equalSessionFour.car, this.driver.Car);
            }

            [Test]
            public void Then_session_is_updated()
            {
                Assert.AreEqual(equalSessionFour.session, this.driver.Session);
            }
        }

        #endregion

        #region Nested type: Given_created_driver_details_When_merging_driver_with_equal_details_from_older_session

        [TestFixture]
        public class Given_created_driver_details_When_merging_driver_with_equal_details_from_older_session
        {
            private Tools.Results.Domain.Driver driver;

            [SetUp]
            public void Setup()
            {
                (string name, uint bestTime, string session, string car, uint totalLaps) = driverDetails;
                this.driver = new Tools.Results.Domain.Driver(name, bestTime, session, car, totalLaps);

                (bestTime, session, car, totalLaps) = equalSessionZero;
                this.driver.Merge(bestTime, session, car, totalLaps);
            }

            [Test]
            public void Then_car_is_unchanged()
            {
                Assert.AreEqual(driverDetails.car, this.driver.Car);
            }

            [Test]
            public void Then_session_is_unchanged()
            {
                Assert.AreEqual(driverDetails.session, this.driver.Session);
            }
        }

        #endregion

        #region Nested type: Given_created_driver_details_When_merging_driver_with_improved_details

        [TestFixture]
        public class Given_created_driver_details_When_merging_driver_with_improved_details
        {
            private Tools.Results.Domain.Driver driver;

            [SetUp]
            public void Setup()
            {
                (string name, uint bestTime, string session, string car, uint totalLaps) = driverDetails;
                this.driver = new Tools.Results.Domain.Driver(name, bestTime, session, car, totalLaps);

                (bestTime, session, car, totalLaps) = improvedSessionTwo;
                this.driver.Merge(bestTime, session, car, totalLaps);
            }

            [Test]
            public void Then_best_time_is_updated()
            {
                Assert.AreEqual(improvedSessionTwo.bestTime, this.driver.BestTime);
            }

            [Test]
            public void Then_car_is_updated()
            {
                Assert.AreEqual(improvedSessionTwo.car, this.driver.Car);
            }

            [Test]
            public void Then_laps_are_summed()
            {
                Assert.AreEqual(driverDetails.totalLaps + improvedSessionTwo.totalLaps, this.driver.TotalLaps);
            }

            [Test]
            public void Then_session_is_updated()
            {
                Assert.AreEqual(improvedSessionTwo.session, this.driver.Session);
            }
        }

        #endregion

        #region Nested type: Given_created_driver_details_When_merging_driver_with_worse_details

        [TestFixture]
        public class Given_created_driver_details_When_merging_driver_with_worse_details
        {
            private Tools.Results.Domain.Driver driver;

            [SetUp]
            public void Setup()
            {
                (string name, uint bestTime, string session, string car, uint totalLaps) = driverDetails;
                this.driver = new Tools.Results.Domain.Driver(name, bestTime, session, car, totalLaps);

                (bestTime, session, car, totalLaps) = worseSessionThree;
                this.driver.Merge(bestTime, session, car, totalLaps);
            }

            [Test]
            public void Then_best_time_is_unchanged()
            {
                Assert.AreEqual(driverDetails.bestTime, this.driver.BestTime);
            }

            [Test]
            public void Then_car_is_unchanged()
            {
                Assert.AreEqual(driverDetails.car, this.driver.Car);
            }

            [Test]
            public void Then_laps_are_summed()
            {
                Assert.AreEqual(driverDetails.totalLaps + improvedSessionTwo.totalLaps, this.driver.TotalLaps);
            }

            [Test]
            public void Then_session_is_unchanged()
            {
                Assert.AreEqual(driverDetails.session, this.driver.Session);
            }
        }

        #endregion

        #region Nested type: Given_driver_details_When_constructing_driver

        [TestFixture]
        public class Given_driver_details_When_constructing_driver
        {
            private Tools.Results.Domain.Driver driver;

            [SetUp]
            public void Setup()
            {
                (string name, uint bestTime, string session, string car, uint totalLaps) = driverDetails;

                this.driver = new Tools.Results.Domain.Driver(name, bestTime, session, car, totalLaps);
            }

            [Test]
            public void Then_best_time_is_correctly_set()
            {
                Assert.AreEqual(driverDetails.bestTime, this.driver.BestTime);
            }

            [Test]
            public void Then_car_is_correctly_set()
            {
                Assert.AreEqual(driverDetails.car, this.driver.Car);
            }

            [Test]
            public void Then_name_is_correctly_set()
            {
                Assert.AreEqual(driverDetails.name, this.driver.Name);
            }

            [Test]
            public void Then_session_is_correctly_set()
            {
                Assert.AreEqual(driverDetails.session, this.driver.Session);
            }

            [Test]
            public void Then_total_laps_is_correctly_set()
            {
                Assert.AreEqual(driverDetails.totalLaps, this.driver.TotalLaps);
            }
        }

        #endregion
    }
}
