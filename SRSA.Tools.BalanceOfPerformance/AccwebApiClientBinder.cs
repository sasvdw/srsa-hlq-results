﻿namespace SRSA.Tools.BalanceOfPerformance;

using System.CommandLine;
using System.CommandLine.Binding;
using Integration.accweb;
using Integration.Accweb;
using Microsoft.Kiota.Abstractions.Authentication;
using Microsoft.Kiota.Http.HttpClientLibrary;

internal class AccwebApiClientBinder : BinderBase<AccwebApiClient>
{
    private readonly Option<Uri> baseUrlOption;
    private readonly Option<string> passwordOption;

    public AccwebApiClientBinder(Option<Uri> baseUrlOption, Option<string> passwordOption)
    {
        this.baseUrlOption = baseUrlOption;
        this.passwordOption = passwordOption;
    }
    protected override AccwebApiClient GetBoundValue(BindingContext bindingContext)
    {
        var baseUri = bindingContext.ParseResult.GetValueForOption(baseUrlOption)!;
        var password = bindingContext.ParseResult.GetValueForOption(passwordOption)!;

        var accessTokenProvider = new AccwebAccessTokenProvider(baseUri, password);

        var requestAdapter = new HttpClientRequestAdapter(new BaseBearerTokenAuthenticationProvider(accessTokenProvider));
        requestAdapter.BaseUrl = baseUri.ToString();
        
        return new AccwebApiClient(requestAdapter);
    }
}
