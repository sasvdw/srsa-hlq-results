﻿namespace SRSA.Tools.BalanceOfPerformance;

using System.CommandLine;
using Integration.Accweb;
using Integration.Accweb.Models.App;
using Integration.Accweb.Models.Instance;
using Newtonsoft.Json;
using Integration.LowFuelMotorsport;
using Integration.SimRacingGP;
using Integration.SimRacingGP.Models.Command.BalanceOfPerformance;
using Raw.ACC;
using Raw.ACC.Definitions;

public static class BalanceOfPerformanceHost
{
    public static Command BuildCommand()
    {
        var lfmToBopCommand = BuildLfmCommand();
        
        var rootCommand = new Command("bop")
        {
            lfmToBopCommand
        };
        rootCommand.Description = "General BoP Tool";

        return rootCommand;
    }

    private static Command BuildLfmCommand()
    {
        var lofFuelMotorsportApiClientBinder = new LowFuelMotorsportApiClientBinder();
        var toAccwebCommand = BuildToAccwebCommand(lofFuelMotorsportApiClientBinder);
        var toSgpCommand = BuildToSgpCommand(lofFuelMotorsportApiClientBinder);
        
        var lfmCommand = new Command("lfm")
        {
            toAccwebCommand,
            toSgpCommand
        };
        
        lfmCommand.Description = "Download LFM BoP";

        lfmCommand.SetHandler(
            ProcessLfmToBop,
            lofFuelMotorsportApiClientBinder
        );

        return lfmCommand;
    }

    private static Command BuildToSgpCommand(LowFuelMotorsportApiClientBinder lowFuelMotorsportApiClientBinder)
    {
        var communityIdArgument = new Argument<string>("The SGP Community ID as found in the URL.");

        var sgpTokenOption = new Option<string>("-sgpToken", "SGP Bearer Access Token")
        {
            IsRequired = true
        };

        var toSgpCommand = new Command("sgp")
        {
            communityIdArgument,
            sgpTokenOption
        };
        toSgpCommand.Description = "Download LFM BoP and upload to SGP Community";
        
        toSgpCommand.SetHandler(
            ProcessLfmToSgp,
            communityIdArgument,
            new SimRacingGPApiClientBinder(sgpTokenOption),
            lowFuelMotorsportApiClientBinder
            );

        return toSgpCommand;
    }

    private static Command BuildToAccwebCommand(LowFuelMotorsportApiClientBinder lowFuelMotorsportApiClientBinder)
    {
        var restartRunningServersOption = new Option<bool>("--restart-running-servers");
        
        var accwebBaseUrlOption = new Option<Uri>("--accweb-base-url", "ACC Web base URL")
        {
            IsRequired = true
        };
        
        var accwebPasswordOption = new Option<string>("--accweb-password", "ACC Web Password")
        {
            IsRequired = true
        };

        var accwebCommand = new Command("accweb")
        {
            restartRunningServersOption,
            accwebBaseUrlOption,
            accwebPasswordOption
        };
        accwebCommand.Description = "Download LFM BoP and upload to Accweb";
        
        accwebCommand.SetHandler(
            ProcessLfmToAccweb,
            restartRunningServersOption,
            lowFuelMotorsportApiClientBinder,
            new AccwebApiClientBinder(accwebBaseUrlOption, accwebPasswordOption)
            );

        return accwebCommand;
    }

    private static async Task ProcessLfmToSgp(string communityId, SimRacingGPApiClient simRacingGpApiClient, LowFuelMotorsportApiClient lfmApiClient)
    {
        var currentSgpBops = await simRacingGpApiClient.Query.Bop[communityId].Acc.GetAsync();
        var currentBopsLookups = currentSgpBops.Where(x => x.TrackId is not null)
            .ToDictionary(x => x.Name);

        var lfmAccBops = await lfmApiClient.Hotlaps.GetAccBop.GetAsync();

        foreach (var lfmAccBop in lfmAccBops)
        {
            var name = $"LFM - {lfmAccBop.TrackName} - V{lfmAccBop.BopVersion}";

            if (currentBopsLookups.TryGetValue(name, out var sgpBop))
            {
                await simRacingGpApiClient.Command.Bop.Delete[sgpBop.Id].PostAsync();
            }
                
            var newBops = new Dictionary<int, NewAccBalanceOfPeformance.Entry>();

            foreach (var entry in lfmAccBop.Bop.Values.SelectMany(x => x))
            {
                var newEntry = new NewAccBalanceOfPeformance.Entry()
                {
                    Ballastkg = entry.Ballast,
                };

                newBops.Add(entry.CarModel, newEntry);
            }
                
            var newSgpBop = new NewAccBalanceOfPeformance()
            {
                Game = "acc",
                LeagueId = communityId,
                Name = name,
                TrackId = Tracks.TrackNamesToIds[lfmAccBop.TrackName],
                Bops = newBops
            };

           var result = await simRacingGpApiClient.Command.Bop.Create.PostAsync(newSgpBop);
           Console.WriteLine(result.Id);
        }
    }

    private static async Task ProcessLfmToBop(LowFuelMotorsportApiClient lfmApiClient)
    {
        var lfmAccBops = await lfmApiClient.Hotlaps.GetAccBop.GetAsync();

        var balanceOfPerformance = new BalanceOfPerformance();

        foreach (var lfmAccBop in lfmAccBops)
        {
            foreach (var entry in lfmAccBop.Bop.Values.SelectMany(x => x))
            {
                var track = Tracks.TrackNamesToIds.GetValueOrDefault(lfmAccBop.TrackName, lfmAccBop.TrackName);
                var bopEntry = new BalanceOfPerformance.Entry
                {
                    Track = track,
                    CarModel = entry.CarModel,
                    BallastKg = entry.Ballast,
                    Restrictor = 0
                };
                
                balanceOfPerformance.Entries.Add(bopEntry);
            }
        }
        
        Console.WriteLine(JsonConvert.SerializeObject(balanceOfPerformance, Formatting.Indented));
    }

    private static async Task ProcessLfmToAccweb(bool restartRunningServers, LowFuelMotorsportApiClient lfmApiClient, AccwebApiClient accwebClient)
    {
        var serversTask = accwebClient.Servers.GetAsync();

        var lfmAccBops = await lfmApiClient.Hotlaps.GetAccBop.GetAsync();
        
        var trackBops = new Dictionary<string, BopJson>();

        foreach (var lfmAccBop in lfmAccBops)
        {
            var entries = new List<BopSettings>();
            if (!Tracks.TrackNamesToIds.TryGetValue(lfmAccBop.TrackName, out var trackId))
            {
                continue;
            }

            foreach (var entry in lfmAccBop.Bop.Values.SelectMany(x => x))
            {
                var bopSettings = new BopSettings()
                {
                    Track = trackId,
                    CarModel = entry.CarModel,
                    BallastKg = entry.Ballast,
                    Restrictor = 0
                };
                
                entries.Add(bopSettings);
            }
            
            var bopJson = new BopJson()
            {
                ConfigVersion = lfmAccBop.BopVersion,
                Entries = entries
            };
            
            trackBops.Add(trackId, bopJson);
        }
        
        var servers = (await serversTask)!;
        
        foreach (var server in servers)
        {
            var instanceId = int.Parse(server.Id!);
            var instance = (await accwebClient.Instance[instanceId].GetAsync())!;

            if (!trackBops.TryGetValue(instance.Acc!.Event!.Track!, out var bopJson) || bopJson.ConfigVersion == instance.Acc.Bop!.ConfigVersion || !restartRunningServers && (instance.IsRunning ?? false))
            {
                continue;
            }

            if (restartRunningServers && (instance.IsRunning ?? false))
            {
                await accwebClient.Instance[instanceId].Stop.PostAsync();
            }

            var savePayload = new SaveInstancePayload
            {
                Acc = new AccConfigFiles
                {
                    Configuration = instance.Acc!.Configuration,
                    Settings = instance.Acc!.Settings,
                    Event = instance.Acc!.Event,
                    EventRules = instance.Acc!.EventRules,
                    Entrylist = instance.Acc!.Entrylist,
                    Bop = bopJson,
                    AssistRules = instance.Acc!.AssistRules
                },
                AccExtraSettings = instance.AccExtraSettings,
                AccWeb = instance.AccWeb
            };

            await accwebClient.Instance[instanceId].PostAsync(savePayload);

            if (restartRunningServers && (instance.IsRunning ?? false))
            {
                await accwebClient.Instance[instanceId].Start.PostAsync();
            }
        }
    }
}