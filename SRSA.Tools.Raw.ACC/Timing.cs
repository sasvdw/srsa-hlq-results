﻿namespace SRSA.Tools.Raw.ACC;

using System.Collections.Generic;
using Newtonsoft.Json;

internal class Timing
{
    [JsonProperty("lastLap")]
    public int LastLap { get; set; }

    [JsonProperty("lastSplits")]
    public List<int> LastSplits { get; set; }

    [JsonProperty("bestLap")]
    public uint BestLap { get; set; }

    [JsonProperty("bestSplits")]
    public List<int> BestSplits { get; set; }

    [JsonProperty("totalTime")]
    public int TotalTime { get; set; }

    [JsonProperty("lapCount")]
    public uint LapCount { get; set; }

    [JsonProperty("lastSplitId")]
    public long LastSplitId { get; set; }
}