﻿namespace SRSA.Tools.Raw.ACC;

using System.Collections.Generic;
using Newtonsoft.Json;

internal class Lap
{
    [JsonProperty("carId")]
    public int CarId { get; set; }

    [JsonProperty("driverIndex")]
    public int DriverIndex { get; set; }

    [JsonProperty("laptime")]
    public int Laptime { get; set; }

    [JsonProperty("isValidForBest")]
    public bool IsValidForBest { get; set; }

    [JsonProperty("splits")]
    public List<int> Splits { get; set; }
}