﻿namespace SRSA.Tools.Raw.ACC;

using System.Collections.Generic;
using Newtonsoft.Json;

internal class LeaderBoardLine
{
    [JsonProperty("car")]
    public Car Car { get; set; }

    [JsonProperty("currentDriver")]
    public CurrentDriver CurrentDriver { get; set; }

    [JsonProperty("currentDriverIndex")]
    public int CurrentDriverIndex { get; set; }

    [JsonProperty("timing")]
    public Timing Timing { get; set; }

    [JsonProperty("missingMandatoryPitstop")]
    public int MissingMandatoryPitstop { get; set; }

    [JsonProperty("driverTotalTimes")]
    public List<double> DriverTotalTimes { get; set; }
}