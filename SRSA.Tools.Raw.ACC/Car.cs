﻿namespace SRSA.Tools.Raw.ACC;

using System.Collections.Generic;
using Newtonsoft.Json;

internal class Car
{
    [JsonProperty("carId")]
    public int CarId { get; set; }

    [JsonProperty("raceNumber")]
    public int RaceNumber { get; set; }

    [JsonProperty("carModel")]
    public ushort CarModel { get; set; }

    [JsonProperty("cupCategory")]
    public int CupCategory { get; set; }

    [JsonProperty("teamName")]
    public string TeamName { get; set; }

    [JsonProperty("drivers")]
    public List<Driver> Drivers { get; set; }
}