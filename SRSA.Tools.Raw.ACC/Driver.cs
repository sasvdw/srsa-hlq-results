﻿namespace SRSA.Tools.Raw.ACC;

using Newtonsoft.Json;

internal class Driver
{
    [JsonProperty("firstName")]
    public string FirstName { get; set; }

    [JsonProperty("lastName")]
    public string LastName { get; set; }

    [JsonProperty("shortName")]
    public string ShortName { get; set; }

    [JsonProperty("playerId")]
    public string PlayerId { get; set; }
}