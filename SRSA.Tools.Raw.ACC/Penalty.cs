﻿namespace SRSA.Tools.Raw.ACC;

using Newtonsoft.Json;

internal class Penalty
{
    [JsonProperty("carId")]
    public int CarId { get; set; }

    [JsonProperty("driverIndex")]
    public int DriverIndex { get; set; }

    [JsonProperty("reason")]
    public string Reason { get; set; }

    [JsonProperty("penalty")]
    public string Name { get; set; }

    [JsonProperty("penaltyValue")]
    public int Value { get; set; }

    [JsonProperty("violationInLap")]
    public int ViolationInLap { get; set; }

    [JsonProperty("clearedInLap")]
    public int ClearedInLap { get; set; }
}