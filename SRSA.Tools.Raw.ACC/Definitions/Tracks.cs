﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace SRSA.Tools.Raw.ACC.Definitions;

public static class Tracks
{
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static class Definitions
    {
        public const string barcelona = nameof(barcelona);
        public const string brands_hatch = nameof(brands_hatch);
        public const string cota = nameof(cota);
        public const string donington = nameof(donington);
        public const string hungaroring = nameof(hungaroring);
        public const string imola = nameof(imola);
        public const string indianapolis = nameof(indianapolis);
        public const string kyalami = nameof(kyalami);
        public const string laguna_seca = nameof(laguna_seca);
        public const string misano = nameof(misano);
        public const string monza = nameof(monza);
        public const string mount_panorama = nameof(mount_panorama);
        public const string nurburgring = nameof(nurburgring);
        public const string nurburgring_24h = nameof(nurburgring_24h);
        public const string oulton_park = nameof(oulton_park);
        public const string paul_ricard = nameof(paul_ricard);
        public const string red_bull_ring = nameof(red_bull_ring);
        public const string silverstone = nameof(silverstone);
        public const string snetterton = nameof(snetterton);
        public const string spa = nameof(spa);
        public const string suzuka = nameof(suzuka);
        public const string valencia = nameof(valencia);
        public const string watkins_glen = nameof(watkins_glen);
        public const string zandvoort = nameof(zandvoort);
        public const string zolder = nameof(zolder);
    }
    
    public static readonly IReadOnlyDictionary<string, string> TrackIdsToNames = new Dictionary<string, string>()
    {
        [Definitions.barcelona]="Circuit de Catalunya",
        [Definitions.brands_hatch]="Brands Hatch Circuit",
        [Definitions.cota]="Circuit Of The Americas",
        [Definitions.donington]="Donington Park",
        [Definitions.hungaroring]="Hungaroring",
        [Definitions.imola]="Autodromo Enzo e Dino Ferrari",
        [Definitions.indianapolis]="Indianapolis",
        [Definitions.kyalami]="Kyalami",
        [Definitions.laguna_seca]="Laguna Seca",
        [Definitions.misano]="Misano",
        [Definitions.monza]="Autodromo Nazionale di Monza",
        [Definitions.mount_panorama]="Mount Panorama Circuit",
        [Definitions.nurburgring]="Nürburgring",
        [Definitions.nurburgring_24h]="Nürburgring Nordschleife 24h",
        [Definitions.oulton_park]="Oulton Park",
        [Definitions.paul_ricard]="Circuit de Paul Ricard",
        [Definitions.red_bull_ring]="Spielberg - Red Bull Ring",
        [Definitions.silverstone]="Silverstone",
        [Definitions.snetterton]="Snetterton",
        [Definitions.spa]="Circuit de Spa Francorchamps",
        [Definitions.suzuka]="Suzuka Circuit",
        [Definitions.valencia]="Circuit Ricardo Tormo",
        [Definitions.watkins_glen]="Watkins Glen",
        [Definitions.zandvoort]="Zandvoort",
        [Definitions.zolder]="Zolder",
    };

    public static readonly IReadOnlyDictionary<string, string> TrackNamesToIds = TrackIdsToNames.ToDictionary(x => x.Value, x => x.Key); 
}